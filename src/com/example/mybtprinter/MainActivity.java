package com.example.mybtprinter;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarActivity;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

@SuppressLint({ "ShowToast", "NewApi" })
public class MainActivity extends ActionBarActivity implements Runnable {

	
	Button print_button;
	 protected static final String TAG = "TAG";
	 private static final int REQUEST_CONNECT_DEVICE = 1;
	 private static final int REQUEST_ENABLE_BT = 2;
	 Button mScan, mPrint, mDisc;
	 BluetoothAdapter mBluetoothAdapter;
	 private UUID applicationUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	 private ProgressDialog mBluetoothConnectProgressDialog;
	 private BluetoothSocket mBluetoothSocket;
	 private String MAC_ADDR ="mac_address";
	 public static final String MyPREFERENCES = "MyPrefs" ;
	 BluetoothDevice mBluetoothDevice; 
	 
	 SharedPreferences sharedpreferences;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		sharedpreferences = this.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		
		print_button = (Button)findViewById(R.id.button1);
		
		

    	mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(MainActivity.this, "Message1", 2000).show();
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(
                        BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent,
                        REQUEST_ENABLE_BT);
            } else {
            	if(!sharedpreferences.contains(MAC_ADDR)){
  				  ListPairedDevices();
  	              Intent connectIntent = new Intent(MainActivity.this,DeviceListActivity.class);
  	              startActivityForResult(connectIntent,REQUEST_CONNECT_DEVICE);
  	              
  				 
  				}
  				  else {
  					  
  					runtest();
  					
  					
  	                
  	                
  	                
  					  
  				  }
            }
        }
    	  
	
			
			
			
            
           
		
		
		
		
		 print_button.setOnClickListener(new View.OnClickListener() {
	            @SuppressLint("ShowToast")
				public void onClick(View mView) {
	            	
	            	sendData("The following snippet will start an operation in a separate thread, then wait for up to 10 seconds for the operation to complete. If the operation does not complete in time, the code will attempt to cancel the operation, then continue on its merry way. Even if the operation cannot be cancelled easily, the parent thread will not wait for the child thread to terminate.");
	            	
	            	    	
	            	
	            }
		 });
		
	}
	
	
	
	public void runtest(){
			String mDeviceAddress =sharedpreferences.getString(MAC_ADDR, "");
          Log.v(TAG, "Coming incoming address " + mDeviceAddress);
          Editor editor = sharedpreferences.edit();
          editor.putString(MAC_ADDR, mDeviceAddress);
          editor.commit();
          mBluetoothDevice = mBluetoothAdapter.getRemoteDevice(mDeviceAddress);
          //mBluetoothConnectProgressDialog = ProgressDialog.show(this,"Connecting...", mBluetoothDevice.getName() + " : "+ mBluetoothDevice.getAddress(), true, false);
		Thread mBlutoothConnectThread = new Thread(this);
		mBlutoothConnectThread.start();
		
	}
	
	
	public void onActivityResult(int mRequestCode, int mResultCode,
            Intent mDataIntent) {
        super.onActivityResult(mRequestCode, mResultCode, mDataIntent);

        switch (mRequestCode) {
        case REQUEST_CONNECT_DEVICE:
            if (mResultCode == Activity.RESULT_OK) {
                Bundle mExtra = mDataIntent.getExtras();
                String mDeviceAddress = mExtra.getString("DeviceAddress");
                Log.v(TAG, "Coming incoming address " + mDeviceAddress);
                Editor editor = sharedpreferences.edit();
                editor.putString(MAC_ADDR, mDeviceAddress);
                editor.commit();
                mBluetoothDevice = mBluetoothAdapter
                        .getRemoteDevice(mDeviceAddress);
                mBluetoothConnectProgressDialog = ProgressDialog.show(this,
                        "Connecting...", mBluetoothDevice.getName() + " : "
                                + mBluetoothDevice.getAddress(), true, false);
                
                Thread mBlutoothConnectThread = new Thread(this);
                mBlutoothConnectThread.start();
                // pairToDevice(mBluetoothDevice); This method is replaced by
                // progress dialog with thread
            }
            break;

        case REQUEST_ENABLE_BT:
            if (mResultCode == Activity.RESULT_OK) {
                ListPairedDevices();
                Intent connectIntent = new Intent(MainActivity.this,DeviceListActivity.class);
                startActivityForResult(connectIntent, REQUEST_CONNECT_DEVICE);
            } else {
                Toast.makeText(MainActivity.this, "Message", 2000).show();
            }
            break;
        }
    }
	
	
	
	
	
	
	
	
	 private void ListPairedDevices() {
	        Set<BluetoothDevice> mPairedDevices = mBluetoothAdapter
	                .getBondedDevices();
	        if (mPairedDevices.size() > 0) {
	            for (BluetoothDevice mDevice : mPairedDevices) {
	                Log.v(TAG, "PairedDevices: " + mDevice.getName() + "  "
	                        + mDevice.getAddress());
	            }
	        }
	    }
	 
	 public void run() {
		
		 
	        try {
	            mBluetoothSocket = mBluetoothDevice.createRfcommSocketToServiceRecord(applicationUUID);
	            Method m = mBluetoothDevice.getClass().getMethod("createRfcommSocket", new Class[] { int.class });
	            mBluetoothSocket = (BluetoothSocket) m.invoke(mBluetoothDevice, 1);
	            
	            Log.e("TAG",mBluetoothDevice.getAddress());
	            mBluetoothAdapter.cancelDiscovery();
	            mBluetoothSocket.connect();
	            
	            mHandler.sendEmptyMessage(0);
	        } catch (IOException eConnectException) {
	            Log.d(TAG, "CouldNotConnectToSocket", eConnectException);
	            closeSocket(mBluetoothSocket);
	            return;
	        } catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
	 }

	    private void closeSocket(BluetoothSocket nOpenSocket) {
	        try {
	            nOpenSocket.close();
	            Log.d(TAG, "SocketClosed");
	        } catch (IOException ex) {
	            Log.d(TAG, "CouldNotCloseSocket");
	        }
	    }

	    private Handler mHandler = new Handler() {
	        @Override
	        public void handleMessage(Message msg) {
	        	
	        		Toast.makeText(MainActivity.this, "Connected", 5000).show();
	        		if(mBluetoothConnectProgressDialog!=null)
	        			mBluetoothConnectProgressDialog.dismiss();
	            
	        }
	    };

	    public static byte intToByteArray(int value) {
	        byte[] b = ByteBuffer.allocate(4).putInt(value).array();

	        for (int k = 0; k < b.length; k++) {
	            System.out.println("Selva  [" + k + "] = " + "0x"
	                    + UnicodeFormatter.byteToHex(b[k]));
	        }

	        return b[3];
	    }

	    public byte[] sel(int val) {
	        ByteBuffer buffer = ByteBuffer.allocate(2);
	        buffer.putInt(val);
	        buffer.flip();
	        return buffer.array();
	    }
	    
	    
	    @Override
	    protected void onDestroy() {
	        // TODO Auto-generated method stub
	        super.onDestroy();
	        try {
	            if (mBluetoothSocket != null)
	                mBluetoothSocket.close();
	        } catch (Exception e) {
	            Log.e("Tag", "Exe ", e);
	        }
	    }
	    public void sendData(final String BILL){
			 
			 
			 Thread t = new Thread() {
	          public void run() {
	              try {
	                  OutputStream os = mBluetoothSocket
	                          .getOutputStream();
	                  os.write(BILL.getBytes());
	                      //This is printer specific code you can comment ==== > Start

	                  // Setting height
	                 

	                  

	//printer specific code you can comment ==== > End
	              } catch (Exception e) {
	                  Log.e("Main", "Exe ", e);
	              }
	          }
	      };
	      t.start();

			 
			 
			 
		 }
		 
	    
	    @Override
	    public void onBackPressed() {
	        try {
	            if (mBluetoothSocket != null)
	                mBluetoothSocket.close();
	        } catch (Exception e) {
	            Log.e("Tag", "Exe ", e);
	        }
	        setResult(RESULT_CANCELED);
	        finish();
	    }
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
}
